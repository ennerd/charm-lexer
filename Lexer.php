<?php
namespace Charm;

use Charm\Lexer\Token;

class Lexer {

    /**
     * Some regex patterns for common use cases
     */
    const DOUBLE_QUOTED_STRING_RE = '\"(\\\\\\\\|\\\\"|[^"])*"';
    const SINGLE_QUOTED_STRING_RE = '\'(\\\\\\\\|\\\\\'|[^\'])*\'';
    const INT_RE = '(?<!\.)\b([1-9][0-9]*|[0-9])\b(?!\.)';
    const FLOAT_RE = '\b(?<!\.)(([1-9][0-9]*|[0-9])\.[0-9]+)(?!\.)\b';
    const NUMBER_RE = '\b(?<!\.)([1-9][0-9]*|[0-9])(\.[0-9]+)?(?!\.)\b';
    const HEX_RE = '\b0[xX][0-9a-fA-F]+\b';
    const OCT_RE = '\b0[0-7]+\b';
    const BIN_RE = '\b0[bB][01]+\b';
    const IDENTIFIER_RE = '\b[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
    const COMMENTS_RE = '\/\*[\s\S]*\*\/|\/\/[^\n]*|#[^\n]*';
    const SEPARATORS_RE = '[;,.:()[\]{}]';
    const WHITESPACE_RE = '\s+';
    const C99_KEYWORDS_RE = 'auto|break|case|char|const|continue|default|do|double|else|enum|extern|float|for|goto|if|inline|'.
        'int|long|register|restrict|return|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|while|'.
        '_Bool|_Complex|_Imaginary';

    /**
     * Map of token kinds to regular expressions. The regular expressions
     * will be used with / as an escape character.
     */
    const DEFAULT_REGEX_PATTERNS = [
        'IDENT' => self::IDENTIFIER_RE,                         // any typical variable name like 'x', 'x123', 'hello_world'
        'LITERAL' =>                                            // numbers in various formats and strings
            self::NUMBER_RE . '|' .                             // integers and floats
            '\b0([bB][01]+|[xX][0-9a-fA-F]+|[0-7]+)\b|' .       // 0XFF 0xff 0xFF 0b01 0B01 0777 (hex, bin and octal)
            self::SINGLE_QUOTED_STRING_RE . '|' .               // 'Single quoted string \' \\'
            self::DOUBLE_QUOTED_STRING_RE,                      // 'Double quoted string \" \\"
        'COMMENT' => self::COMMENTS_RE,                         // /* comment */ // comment and # comment
    ];

    /**
     * Array of strings which are matched character by character.
     */
    const DEFAULT_STRING_PATTERNS = [
        'TOKEN' => [
            '=', '+=', '-=', '*=', '/=', '%=', '&=', '|=', '^=', '<<=', '>>=',  // C assignment operators
            '[', ']', '->', '...', '.',                                         // member access (also * and &)
            '++', '--',                                                         // increment/decrement
            '+', '-', '*', '/', '%', '~', '&', '|', '^', '<<', '>>',            // arithmetic
            '!', '&&', '||',                                                    // logical
            '==', '!=', '<=', '>=', '<', '>',                                   // comparison
            '(', ')', ',', '?', ':',                                            // other tokens
        ],
    ];

    /**
     * @var array<string, array<string>> Map from kind to regex patterns
     */
    private $patterns = [];

    /**
     * @var string Regex pattern for whitespace which will removed from the token stream.
     */
    private $whitespace = null;

    /**
     * @var callable Callback which does the tokenizing of the source string and returns a generator for tokens.
     */
    private $tokenizer;

    public function __construct(
        array $regexPatterns=self::DEFAULT_REGEX_PATTERNS,
        array $stringPatterns=self::DEFAULT_STRING_PATTERNS,
        ?string $whitespacePattern=self::WHITESPACE_RE) {

        foreach ($regexPatterns as $kind => $patterns) {
            if (is_array($patterns)) {
                foreach ($patterns as $pattern) {
                    $this->addRegexPattern($kind, $pattern);
                }
            } else {
                $this->addRegexPattern($kind, $patterns);
            }
        }

        foreach ($stringPatterns as $kind => $patterns) {
            if (is_array($patterns)) {
                foreach ($patterns as $pattern) {
                    $this->addStringPattern($kind, $pattern);
                }
            } else {
                $this->addStringPattern($kind, $patterns);
            }
        }

        $this->whitespace = $whitespacePattern;
    }

    /**
     * Add a terminal kind with a name and a regex pattern. Multiple kinds
     * can be associated with the same name.
     *
     * @param $kind The name of the token
     * @param $regex The regex pattern (without opening/closing slash)
     */
    public function addRegexPattern(string $kind, string $pattern) {
        $this->patterns[$kind][] = $pattern;
    }

    /**
     * Add a terminal symbol, which is matched exactly as the string provided.
     * For example "while" or "+". No regular expression. Will be returned as
     * a 'token' kind.
     */
    public function addStringPattern(string $kind, string $pattern) {
        $this->addRegexPattern($kind, preg_quote($pattern, "/"));
    }

    /**
     * Returns an iterator which yields tokens as parsed from the
     * input. Strings, iterators, stringable objects and streams
     * are supported sources.
     */
    public function tokenize($source): iterable {
        if (is_string($source)) {
            return $this->tokenizeFromGenerator(function() use ($source) {
                yield $source;
            });
        } elseif (is_iterable($source)) {
            return $this->tokenizeFromGenerator(function() use ($source) {
                yield from $source;
            });
        } elseif (is_object($source) && method_exists($source, '__toString')) {
            return $this->tokenizeFromGenerator(function() use ($source) {
                yield (string) $source;
            });
        } elseif (is_callable($source)) {
            return $this->parse($source());
        } elseif (is_resource($source)) {
            switch (get_resource_type($source)) {
                case 'stream' :
                    return $this->tokenizeFromGenerator(function() use ($source) {
                        while (!feof($source)) {
                            $chunk = fread($source, 65536);
                            if (!is_string($chunk)) {
                                return;
                            }
                            yield $chunk;
                        }
                    });
                default :
                    throw new Exception("Unknown resource type '" . get_resource_type($source) . "'");
            }
        } else {
            throw new Exception("Unsupported source type");
        }
    }

    /**
     * Accepts a callback which returns a generator function. Will iterate the
     * generator and expect string chunks to be yielded. Tokens will be returned
     * as soon as they are identified.
     *
     * @param $source Callback returning a generator which yields the string to tokenize
     * @return iterator<Token>
     */
    public function tokenizeFromGenerator(callable $source) {
        $tokenizer = $this->buildTokenizer();
        yield from $tokenizer($source);
    }

    private function buildTokenizer() {
        $regexes = [];
        foreach ($this->patterns as $kind => $expressions) {
            $regexes[] = '(?<'.$kind.'>'.implode("|", $expressions).')';
        }
        if ($this->whitespace !== null) {
            $regexes[] = '(?<__WS__>'.$this->whitespace.')';
        }
        $regexes[] = '(?<__FAIL__>.+?)';

        $regex = '/'.implode('|', $regexes).'/uA';
        return function(callable $chunkGenerator) use ($regex) {
            $offset = 0;
            $buffer = '';
            $finalChunk = false;

            $tokenizer = function($matches) use (&$buffer, &$offset) {
                $rebuffer = '';
                foreach ($matches as $index => $match) {
                    if ($rebuffer !== '') {
                        // we started buffering
                        $rebuffer .= $match[0];
                    } elseif (!empty($match['__FAIL__'])) {
                        $rebuffer = $match[0];
                        // to avoid generating tokens from partial matches, we'll start buffering
                    } else {
                        $kind = array_search($match[0], array_slice($match, 1, null, true), true);
                        if ($kind !== '__WS__') {
                            // whitespace is always discarded
                            yield new Token($match[0], $kind, $offset);
                        }
                        $offset += strlen($match[0]);
                    }
                }
            };

            foreach ($chunkGenerator() as $chunk) {
                if ($chunk === '') {
                    usleep(0);
                    continue;
                }
                $buffer .= $chunk;
                if (strlen($buffer) < 4096) {
                    // try to get more data
                    continue;
                }
                $buffer = '';
                yield from $tokenizer($matches);
                $matches = null;
            }
            if ($buffer !== '') {
                preg_match_all($regex, $buffer, $matches, PREG_SET_ORDER);
                yield from $tokenizer($matches);
            }
        };
    }

    private function buildRegex() {
        $tokens = $this->tokens;
        usort($tokens, function($a, $b) {
            return strlen($b) - strlen($a);
        });
        $tokens = array_map(function($t) {
            return preg_quote($t, '/');
        }, $tokens);
        $finalPatterns = [
            'TOKEN' => [ implode("|", $tokens) ],
        ];
        foreach ($this->patterns as $kind => $patterns) {
            foreach ($patterns as $pattern) {
                $finalPatterns[$kind][] = $pattern;
            }
        }
        foreach ($finalPatterns as $kind => &$patterns) {
            $patterns = implode("|", $patterns);
            $patterns = '(?<'.$kind.'>'.$patterns.')';
        }
        $patterns[] = '(?<__FAIL__>.)';
        $this->regex = '/'.implode("|", $patterns).'/';
    }
}
