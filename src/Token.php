<?php
namespace Charm\Lexer;

class Token {

    public $content;
    public $kind;
    public $offset;

    public function __construct(string $content, string $kind, int $offset) {
        $this->content = $content;
        $this->kind = $kind;
        $this->offset = $offset;
    }

}
